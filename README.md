# Vector Semantics

A word2vec implementation with Python. Used to find the meaning and relation between words based on the context they appear in.

## Table of Contents

* [Table of Contents](#table-of-contents)
* [Project Statement](#project-statement)
* [Getting started](#getting-started)
    + [Prerequisites](#prerequisites)
    + [Installing](#installing)
* [Usage](#usage)
* [Running the Tests](#running-the-tests)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgements](#acknowledgements)


## Project Statement

This project serves as an exploration of the word2vec language model, which is used to create vectorial representations of words, also known as word embeddings. This can be used to analyze semantic properties of words, such as finding the degree of similarity between two words; by applying mathematical operations to these vectors.

To demonstrate this, three datasets where used; one with sentences related to **streaming** commands, one with **preprogramming** related commands and one with **TV control** commands.

The demonstration can be found on the Jupyter Notebook `word2vec` in the project root directory.

## Getting started

### Prerequisites

In order to run this project, an installation of Python 3.5 is required. Follow instructions for your operating system in case you don't have this.

### Installing

Once Python 3.5 is installed, clone this repository and open up a terminal in the root directory of the project. Dependency management is done with a combination of pip and virtual environments. To start, activate the virtual environment for this project with the following command (for Unix-based systems):

```
source venv/bin/activate
```

Then, download all dependencies for the project with the following command:

```
pip install -r requirements.txt
```

Once the installation completes, you are ready to run the project.

## Usage

This project uses a Jupyter notebook to demonstrate the training of the neural network, as well as its results. To start Jupyter, execute the following command from the project root:

```
jupiter notebook
```

Then, open up a browser with the host shown on the console. This will open the Jupyter GUI. From there, open the file `word2vec.ipynb` and follow the instructions on it.

## Running the Tests

Tests were added to the implementation of Word2Vec. This tests cover most of the logic included in that class that doesn't require training. To run them, execute the following command from the project's root directory:

```
python -m unittest src/test/word2vec_test.py
```


## Built With

- [Python 3](https://www.python.org/)
- [TensorFlow](https://www.tensorflow.org/)
- [Gensim](https://radimrehurek.com/gensim/)
- [scikit-learn](https://scikit-learn.org/stable/)
- [Jupyter](https://jupyter.org/)
- [NumPy](https://www.numpy.org/)

## Authors

- Aarón Rosas
- Alejandro Del Villar
- Flavio Moreno
- Jesús Campos
- Luis Daniel Ortega

## License

This project is licensed under the terms of the MIT license

## Acknowledgements

- Tomas Mikolov, for his work describing the language model implemented.
- [Introduction to Word Embedding and Word2Vec](https://towardsdatascience.com/introduction-to-word-embedding-and-word2vec-652d0c2060fa), for the code examples.

