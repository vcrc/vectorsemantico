import mock
import unittest
from ..word2vec import Word2Vec
from ..vocabulary import Vocabulary

class Word2VecTest(unittest.TestCase):

    # If the neural network hasn't ben trained, returns None
    def test_get_word_vector_before_training(self):
        w2v = Word2Vec(None, None)
        self.assertIsNone(w2v.get_word_vector('any'))

    # If the neural network hasn't ben trained, returns None
    def test_find_closest_word_before_training(self):
        w2v = Word2Vec(None, None)
        self.assertIsNone(w2v.find_closest_word('any'))

    # The resulting one hot vector is of the length of the vocabulary
    # and the index of the word is a 1
    def test_word_to_one_hot_vec(self):
        test_vocab = Vocabulary()
        test_vocab.add_word('word0')
        test_vocab.add_word('word1')
        test_vocab.add_word('word2')
        
        result = Word2Vec.word_to_one_hot_vec('word2', test_vocab)

        self.assertEqual(len(result), 3)
        self.assertListEqual([0, 0, 1], result.tolist())

    



if __name__ == '__main__':
    unittest.main()