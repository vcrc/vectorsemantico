import re

class Vocabulary:

    def __init__(self):
        self.__words = dict()
        self.__values = dict()
        self.__length = 0
        
    def get_length(self):
        return self.__length

    def get_word_from_value(self, value):
        if value in self.__words:
            return self.__words[value]
        
        return None

    def get_value_from_word(self, word):
        if word.lower() in self.__values:
            return self.__values[word.lower()]
        
        return None

    def get_words(self):
        return list(self.__words.values())

    def load_from_dict(self, vocabulary):
        # Initialize new dictionaries to create a new reference and avoid unwanted changes
        self.__values = vocabulary['values'].copy()
        self.__words = { value: word for word, value in self.__values.items() }
        self.__length = len(self.__words)
    
    def export_to_dict(self):
        # Initialize new dictionaries to create a new reference and avoid unwanted changes
        return { 'words': self.__words.copy(), 'values': self.__values.copy() }

    def create_from_text(self, text):
        # Add all tokens first and remove them from the string
        tokens = set(self.__parse_tokens_from_string(text))
        for token in tokens:
            self.add_word(token)
            text = text.replace(token, '')

        # Get all words from text, remove non alphanumeric characters and add them to vocabulary
        words = text.replace('\n', ' ').split()
        for word in words:
            self.add_word(self.__remove_non_alphanumeric(word))


    def add_word(self, word):
        if word is None or word == '':
            return
        if word not in self.__values:
            word = word.lower()
            self.__words[self.__length] = word
            self.__values[word] = self.__length
            self.__length += 1

    @staticmethod
    def __parse_tokens_from_string(string):
        return re.findall(r'\[(.*?)\]', string)

    @staticmethod
    def __remove_non_alphanumeric(string):
        return re.sub(r'\W+', '', string)