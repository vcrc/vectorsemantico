import numpy as np
import tensorflow as tf

class Word2Vec:

    def __init__(self, sentences, vocabulary):
        self.sentences = sentences
        self.vocabulary = vocabulary
        self.__vectors = None
        
    def run(self, window_size, embedding_dimension, training_iterations):
        x_training_data = []
        y_training_data = []

        for nearby_words in Word2Vec.__get_nearby_words_tuples(self.sentences, window_size):
            x_training_data.append(Word2Vec.word_to_one_hot_vec(nearby_words[0], self.vocabulary))
            y_training_data.append(Word2Vec.word_to_one_hot_vec(nearby_words[1], self.vocabulary))

        x_training_data = np.asarray(x_training_data)
        y_training_data = np.asarray(y_training_data)

        print('Starting training\n')
        self.__train_tensor_flow_model(x_training_data, y_training_data, embedding_dimension, training_iterations)
        print('\nFinished training!')

    def __train_tensor_flow_model(self, x_training_data, y_training_data, embedding_dimension, training_iterations):
        # Make placeholders for x and y
        x = tf.placeholder(tf.float32, shape=(None, self.vocabulary.get_length()))
        y_label = tf.placeholder(tf.float32, shape=(None, self.vocabulary.get_length()))

        W1 = tf.Variable(tf.random_normal([self.vocabulary.get_length(), embedding_dimension]))
        b1 = tf.Variable(tf.random_normal([embedding_dimension]))
        hidden_representation = tf.add(tf.matmul(x, W1), b1)

        W2 = tf.Variable(tf.random_normal([embedding_dimension, self.vocabulary.get_length()]))
        b2 = tf.Variable(tf.random_normal([self.vocabulary.get_length()]))
        
        prediction = tf.nn.softmax(tf.add(tf.matmul(hidden_representation, W2), b2))


        # Initialize TF
        session = tf.Session()
        init = tf.global_variables_initializer()
        session.run(init)

        # Use a TensorFlow provided loss function
        cross_entropy_loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_label, logits = prediction, axis=-1, name=None)

        # Define the training step:
        train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cross_entropy_loss)

        for i in range(training_iterations):
            session.run(train_step, feed_dict={x: x_training_data, y_label: y_training_data})
            session.run(cross_entropy_loss, feed_dict={x: x_training_data, y_label: y_training_data})

        # Set the result
        self.__vectors = session.run(W1 + b1)

    def get_word_vector(self, word):
        if self.__vectors is None:
            print('Neural network has not been trained yet!')
            return

        word_value = self.vocabulary.get_value_from_word(word)
        if word_value is None:
            print('Word', word, 'is not part of the vocabulary')
            return

        return self.__vectors[word_value]

    def find_closest_word(self, word):
        if self.__vectors is None:
            print('Neural network has not been trained yet!')
            return 

        query_word_value = self.vocabulary.get_value_from_word(word)
        if query_word_value is None:
            print('Word', query_word_value, 'is not part of the vocabulary')
            return

        query_vector = self.__vectors[query_word_value]

        _vectors = list(map(lambda vector: [vector[0], vector[1]], enumerate(self.__vectors)))
        _vectors = list(filter(lambda vector: not np.array_equal(query_vector, vector[1]), _vectors))
        closest_word_value, vector = min(_vectors, key=lambda vector: self.__euclidean_distance(vector[1], query_vector))

        return self.vocabulary.get_word_from_value(closest_word_value)

    @staticmethod
    def __get_nearby_words_tuples(sentences, window_size):
        data = []
        
        for sentence in sentences:
            for index, word in enumerate(sentence):
                for nearby_word in sentence[max(index - window_size, 0) : min(index + window_size, len(sentence)) + 1]: 
                    if nearby_word != word:
                        data.append((word, nearby_word))
        
        return data

    @staticmethod
    def word_to_one_hot_vec(word, vocabulary):
        one_hot_vec = np.zeros(vocabulary.get_length())
        one_hot_vec[vocabulary.get_value_from_word(word)] = 1
        return one_hot_vec

    @staticmethod
    def __euclidean_distance(vec1, vec2):
        return np.sqrt(np.sum((vec1-vec2)**2))
    